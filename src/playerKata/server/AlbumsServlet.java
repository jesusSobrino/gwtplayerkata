package playerKata.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import playerKata.client.model.Album;
import playerKata.client.model.Song;

public class AlbumsServlet extends HttpServlet {

	private static final long serialVersionUID = 8032611514671727168L;

	private static List<Album> albums = new LinkedList<Album>();

	static {
		List<Song> songsAlbum1 = new ArrayList<>();
		songsAlbum1
				.add(new Song(
						"1",
						"Welcome to the Jungle",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/01%20-%20Welcome%20To%20The%20Jungle.mp3"));
		songsAlbum1
				.add(new Song(
						"2",
						"It's so Easy",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/02%20-%20It's%20So%20Easy.mp3"));
		songsAlbum1
				.add(new Song(
						"3",
						"Nightrain",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/03%20-%20Nightrain.mp3"));
		songsAlbum1
				.add(new Song(
						"4",
						"Out ta Get Me",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/04%20-%20Out%20Ta%20Get%20Me.mp3"));
		songsAlbum1
				.add(new Song(
						"5",
						"Mr. Brownstone",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/05%20-%20Mr.%20Brownstone.mp3"));
		songsAlbum1
				.add(new Song(
						"6",
						"Paradise City",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/04%20-%20Out%20Ta%20Get%20Me.mp3"));

		Album album1 = new Album(
				"1",
				"Appetite for Destruction",
				"Guns N Roses",
				"https://orpheus-bqreaders.s3.amazonaws.com/artists/Guns%20N'%20Roses/Appetite%20For%20Destruction/cover.jpg",
				songsAlbum1);

		List<Song> songsAlbum2 = new ArrayList<>();
		songsAlbum2
				.add(new Song(
						"13",
						"Red hands",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Walk%20Off%20the%20Earth/R.E.V.O.%20-%20EP/01%20-%20Red%20Hands.mp3"));
		songsAlbum2
				.add(new Song(
						"14",
						"Gang of Rhythm",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Walk%20Off%20the%20Earth/R.E.V.O.%20-%20EP/02%20-%20Gang%20of%20Rhythm.mp3"));

		songsAlbum2
				.add(new Song("15", "Speeches",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Walk%20Off%20the%20Earth/R.E.V.O.%20-%20EP/03%20-%20Speeches.mp3"));

		songsAlbum2
				.add(new Song(
						"16",
						"Summer Vibe",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Walk%20Off%20the%20Earth/R.E.V.O.%20-%20EP/04%20-%20Summer%20Vibe.mp3"));

		Album album2 = new Album(
				"2",
				"R.E.V.O.",
				"Walk off the earth",
				"https://orpheus-bqreaders.s3.amazonaws.com/artists/Walk%20Off%20the%20Earth/R.E.V.O.%20-%20EP/cover.jpg",
				songsAlbum2);

		List<Song> songsAlbum3 = new ArrayList<>();
		songsAlbum3
				.add(new Song("17", "Jacqueline",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Franz%20Ferdinand/Franz%20Ferdinand/01%20-%2001%20Jacqueline.mp3"));
		songsAlbum3
				.add(new Song("18", "Auf Achse",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Franz%20Ferdinand/Franz%20Ferdinand/05%20-%2005%20Auf%20Achse.mp3"));
		songsAlbum3
				.add(new Song(
						"19",
						"Cheating on You",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Franz%20Ferdinand/Franz%20Ferdinand/06%20-%2006%20Cheating%20On%20You.mp3"));
		songsAlbum3
				.add(new Song("20", "This Fire",
						"https://orpheus-bqreaders.s3.amazonaws.com/artists/Franz%20Ferdinand/Franz%20Ferdinand/07%20-%2007%20This%20Fire.mp3"));

		Album album3 = new Album("3", "Franz Ferdinand", "Franz Ferdinand",
				"https://orpheus-bqreaders.s3.amazonaws.com/artists/Franz%20Ferdinand/Franz%20Ferdinand/cover.jpg",
				songsAlbum3);

		albums.add(album1);
		albums.add(album2);
		albums.add(album3);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {

			JSONObject responseObj = new JSONObject();

			List<JSONObject> productObjects = new LinkedList<JSONObject>();

			for (Album album : albums) {

				JSONObject productObj = new JSONObject();

				productObj.put("id", album.getId());
				productObj.put("title", album.getTittle());
				productObj.put("artist", album.getArtist());
				productObj.put("coverUrl", album.getCoverUrl());

				List<JSONObject> songsObject = new LinkedList<JSONObject>();
				for (Song song : album.getSongs()) {
					JSONObject songObj = new JSONObject();
					songObj.put("id", song.getId());
					songObj.put("title", song.getTittle());
					songObj.put("url", song.getUrl());
					songsObject.add(songObj);
				}

				productObj.put("songs", songsObject);

				productObjects.add(productObj);
			}

			responseObj.put("albums", productObjects);

			PrintWriter writer = resp.getWriter();
			writer.write(String.valueOf(responseObj));
			writer.flush();

		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e);
		}

	}

}