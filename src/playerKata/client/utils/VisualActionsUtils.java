package playerKata.client.utils;

public class VisualActionsUtils {

	public static native void cleanElements(String classItem, String classToClear)/*-{
		var elements = $doc.getElementsByClassName(classItem);

		for (var i = 0; i < elements.length; i++) {
			var element = elements[i];
			var cls = classToClear;
			if (element.className
					.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))) {
				var exp = new RegExp('(\\s|^)' + cls + '(\\s|$)');
				element.className = element.className.replace(exp, "");
			}
		}
	}-*/;

}
