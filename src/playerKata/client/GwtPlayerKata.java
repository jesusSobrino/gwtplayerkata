package playerKata.client;

import java.util.List;

import playerKata.client.model.Album;
import playerKata.client.view.AlbumListView;
import playerKata.client.view.AlbumView;
import playerKata.client.view.PlayerView;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GwtPlayerKata implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad() {
		// Se parsea el Json a través de un Servlet
		fetchDataFromServer();
	}

	/**
	 * Llamada al servlet
	 * 
	 * @return List<Album> albums
	 */
	private void fetchDataFromServer() {

		try {

			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, "/gwtPlayerKata/albums.json");

			rb.setCallback(new RequestCallback() {
				@Override
				public void onResponseReceived(Request request, Response response) {
					List<Album> listAlbums = JsonParser.parseJsonData(response.getText());
					initViews(listAlbums);
				}

				@Override
				public void onError(Request request, Throwable exception) {
					Window.alert(SERVER_ERROR + exception.getMessage());
				}
			});
			rb.send();

		} catch (RequestException e) {
			Window.alert(SERVER_ERROR + e.getMessage());
		}
	}

	/**
	 * 
	 * @param listAlbums
	 */
	private void initViews(List<Album> listAlbums) {
		RootPanel rigthPanel = RootPanel.get("rightPanel");
		RootPanel leftPanel = RootPanel.get("leftPanel");

		PlayerView playerView = new PlayerView();
		AlbumView albumView = new AlbumView(listAlbums, playerView);
		AlbumListView albumListView = new AlbumListView(listAlbums, albumView, playerView);
		Label albumTitleTop = new Label("Playlist");
		albumTitleTop.addStyleName("leftText h1");
		leftPanel.add(albumTitleTop);
		leftPanel.add(playerView);
		leftPanel.add(albumView);
		rigthPanel.add(albumListView);
	}
}
