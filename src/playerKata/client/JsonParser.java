package playerKata.client;

import java.util.ArrayList;
import java.util.List;

import playerKata.client.model.Album;
import playerKata.client.model.Song;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

/**
 * 
 * @author jesus
 * 
 */
public class JsonParser {

	/**
	 * Se parsea el Json obtenido a través de un Servlet
	 * 
	 * @param json
	 */
	protected static List<Album> parseJsonData(String json) {

		List<Album> listAlbums = new ArrayList<Album>();

		JSONValue value = JSONParser.parseStrict(json);
		JSONObject albumsObj = value.isObject();
		JSONArray albumsArray = albumsObj.get("albums").isArray();

		for (int i = 0; i < albumsArray.size(); i++) {
			JSONObject albumObj = albumsArray.get(i).isObject();
			List<Song> listSongs = new ArrayList<Song>();

			JSONArray songsArray = albumObj.get("songs").isArray();
			if (songsArray != null) {
				for (int j = 0; j < songsArray.size(); j++) {
					JSONObject songObj = songsArray.get(j).isObject();
					Song song = new Song(jsonString(songObj.get("id")), jsonString(songObj.get("title")),
							jsonString(songObj.get("url")));

					listSongs.add(song);
				}
			}

			Album album = new Album(jsonString(albumObj.get("id")), jsonString(albumObj.get("title")),
					jsonString(albumObj.get("artist")), jsonString(albumObj.get("coverUrl")), listSongs);

			listAlbums.add(album);
		}
		return listAlbums;
	}

	/**
	 * 
	 * @param jsonValue
	 * @return String Value
	 */
	protected static String jsonString(JSONValue jsonValue) {
		return ((JSONString) jsonValue).stringValue();
	}
}
