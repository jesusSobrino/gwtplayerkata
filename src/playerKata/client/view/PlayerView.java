package playerKata.client.view;

import playerKata.client.model.Album;
import playerKata.client.model.Song;
import playerKata.client.presentation.PlayerPresentation;
import playerKata.client.utils.VisualActionsUtils;
import playerKata.client.view.contracts.IPlayerView;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PlayerView extends Composite implements IPlayerView {

	private VerticalPanel mainPanel;
	private HorizontalPanel buttonPanel;
	private Album album;
	private Image cover;
	private int volume;
	private Song currentSong;
	private HTML audio;

	public PlayerView() {
		new PlayerPresentation(this);
	}

	@Override
	public void play(Song song) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateSong(Song currentSong) {
		updateSong(currentSong.getUrl());
		this.currentSong = currentSong;
	}

	@Override
	public void updatePlayer(Album album) {
		cover.setUrl(album.getCoverUrl());
		this.album = album;
	}

	@Override
	public void initialize() {
		mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		cover = new Image("http://orpheus-bqreaders.s3.amazonaws.com/products/OrpheusIcon.png");
		cover.addStyleName("cover");
		buttonPanel = new HorizontalPanel();

		VerticalPanel coverButtonsPanel = new VerticalPanel();
		coverButtonsPanel.addStyleName("coverButtonsPanel");
		coverButtonsPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		coverButtonsPanel.add(cover);
		coverButtonsPanel.add(buttonPanel);

		mainPanel.add(coverButtonsPanel);
		audio = new HTML("<audio id='audioPlayer'/>");
		mainPanel.add(audio);
		initializeControls();
	}

	private void initializeControls() {
		Button play = new Button("Play/Pause");
		Button prev = new Button("Prev");
		Button next = new Button("Next");
		Button mute = new Button("Mute");

		buttonPanel.add(prev);
		buttonPanel.add(play);
		buttonPanel.add(next);
		buttonPanel.add(mute);

		play.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				playSong();
			}
		});
		prev.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				previousSong();
			}
		});
		next.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				nextSong();
			}
		});
		mute.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mute();
			}
		});
	}

	/**
	 * Devuelve el índice de canción actual
	 * 
	 * @return
	 */
	private int getCurrentTrack() {
		int indice = -1;
		for (int i = 0; i < album.getSongs().size(); i++) {
			Song song = album.getSongs().get(i);
			if (currentSong.equals(song)) {
				indice = i;
				break;
			}
		}
		return indice;
	}

	/**
	 * Reproduce la canción siguiente
	 */
	private void nextSong() {
		int selectedindex = getCurrentTrack();
		if (selectedindex < album.getSongs().size() - 1) {
			updateSong(album.getSongs().get(selectedindex + 1));
		}

		VisualActionsUtils.cleanElements("cleanItem", "currentSong");
	}

	/**
	 * Reproduce la canción anterior
	 */
	private void previousSong() {
		int selectedindex = getCurrentTrack();
		if (selectedindex > 0) {
			updateSong(album.getSongs().get(selectedindex - 1));
		}

		VisualActionsUtils.cleanElements("cleanItem", "currentSong");
	}

	private native void mute() /*-{
								var audioPlayer = $doc.getElementById("audioPlayer");
								if (!audioPlayer.muted) {
								audioPlayer.muted = true;
								} else {
								audioPlayer.muted = false;
								}
								}-*/;

	private native void playSong() /*-{
									var audioPlayer = $doc.getElementById("audioPlayer");
									if (audioPlayer.paused) {
									audioPlayer.play();
									} else {
									audioPlayer.pause();
									}
									}-*/;

	private native void updateSong(String song) /*-{
												var audioPlayer = $doc.getElementById("audioPlayer")
												audioPlayer.src = song;
												this.@playerKata.client.view.PlayerView::playSong()();
												}-*/;

}
