/**
 * 
 */
package playerKata.client.view.contracts;

import java.util.List;

import playerKata.client.model.Album;

/**
 * @author jesus
 *
 */
public interface IAlbumListView {
	/**
	 * 
	 * @param albums
	 */
	public void fillDataAlbumList(List<Album> albums);
}
