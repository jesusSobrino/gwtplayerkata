/**
 * 
 */
package playerKata.client.view.contracts;

import java.util.List;

import playerKata.client.model.Album;

/**
 * @author jesus
 *
 */
public interface IAlbumView {
	/**
	 * 
	 * @param album
	 */
	public void updateAlbumAndSongs(Album album);
	
	/**
	 * 
	 * @param albums
	 */
	public void initialize();

}
