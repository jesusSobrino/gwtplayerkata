package playerKata.client.view.contracts;

import playerKata.client.model.Album;
import playerKata.client.model.Song;

public interface IPlayerView {

	public void play(Song song);

	public void updateSong(Song currentSong);

	public void updatePlayer(Album album);

	public void initialize();
}
