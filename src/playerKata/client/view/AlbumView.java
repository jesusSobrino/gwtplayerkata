package playerKata.client.view;

import java.util.List;

import playerKata.client.model.Album;
import playerKata.client.model.Song;
import playerKata.client.presentation.AlbumPresentation;
import playerKata.client.utils.VisualActionsUtils;
import playerKata.client.view.contracts.IAlbumView;
import playerKata.client.view.contracts.IPlayerView;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AlbumView extends Composite implements IAlbumView {

	private VerticalPanel mainPanel;
	private IPlayerView playerI;

	public AlbumView(List<Album> listAlbums, IPlayerView playerI) {
		new AlbumPresentation(this);
		this.playerI = playerI;
	}

	@Override
	public void updateAlbumAndSongs(final Album album) {

		mainPanel.clear();

		HorizontalPanel titlePanel = new HorizontalPanel();
		Label albumTitle = new Label(album.getTittle());
		Label albumArtist = new Label(album.getArtist());
		albumTitle.addStyleName("leftText");
		albumArtist.addStyleName("artistLabel");
		titlePanel.add(albumTitle);
		titlePanel.add(albumArtist);
		mainPanel.add(titlePanel);

		List<Song> songs = album.getSongs();
		for (int i = 0; i < songs.size(); i++) {
			final Song song = songs.get(i);
			final Label songTitle = new Label((i + 1) + ".  " + song.getTittle());
			songTitle.addStyleName("albumSongs pointerHand leftText selectableSong cleanItem");
			mainPanel.add(songTitle);

			songTitle.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					VisualActionsUtils.cleanElements("cleanItem", "currentSong");
					selectItem(songTitle);
					updatePlayer(song);
				}
			});
		}
	}

	/**
	 * 
	 * @param song
	 */
	private void updatePlayer(Song currentSong) {
		playerI.updateSong(currentSong);
	}

	private void selectItem(Label selectedItem) {
		selectedItem.addStyleName("currentSong");
	}

	@Override
	public void initialize() {
		mainPanel = new VerticalPanel();

		this.initWidget(mainPanel);
		mainPanel.addStyleName("albumView");

	}

}
