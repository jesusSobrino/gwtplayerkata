package playerKata.client.view;

import java.util.List;

import playerKata.client.model.Album;
import playerKata.client.presentation.AlbumListPresentation;
import playerKata.client.view.contracts.IAlbumListView;
import playerKata.client.view.contracts.IAlbumView;
import playerKata.client.view.contracts.IPlayerView;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AlbumListView extends Composite implements IAlbumListView {

	private VerticalPanel mainPanel;
	private IAlbumView albumI;
	private IPlayerView playerI;

	public AlbumListView(List<Album> listAlbums, IAlbumView albumI, IPlayerView playerI) {
		new AlbumListPresentation(this, listAlbums);
		this.albumI = albumI;
		this.playerI = playerI;
	}

	@Override
	public void fillDataAlbumList(List<Album> albums) {
		mainPanel = new VerticalPanel();
		this.initWidget(mainPanel);
		this.addStyleName("albumListView");

		Label albumTitle = new Label("Music Library");
		albumTitle.addStyleName("albumTittle h1");
		mainPanel.add(albumTitle);

		VerticalPanel albumPanel = new VerticalPanel();

		for (final Album album : albums) {
			HorizontalPanel songPanel = new HorizontalPanel();
			songPanel.addStyleName("spacing");
			Label labelAlbumTitle = new Label(album.getTittle());
			Label labelAlbumArtist = new Label(album.getArtist());
			Button addButton = new Button("+");
			labelAlbumTitle.addStyleName("albumListViewText");
			labelAlbumArtist.addStyleName("albumListViewText artistLabel");
			addButton.addStyleName("pointerHand");
			addButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					updateAlbum(album);
					updatePlayer(album);

				}
			});
			songPanel.add(addButton);
			songPanel.add(labelAlbumTitle);
			songPanel.add(labelAlbumArtist);
			albumPanel.add(songPanel);
		}

		mainPanel.add(albumPanel);
	}

	/**
	 * 
	 * @param album
	 */
	private void updateAlbum(final Album album) {
		albumI.updateAlbumAndSongs(album);
	}

	private void updatePlayer(Album album) {
		playerI.updatePlayer(album);
	}
}
