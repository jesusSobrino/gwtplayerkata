package playerKata.client.presentation;

import java.util.List;

import playerKata.client.model.Album;
import playerKata.client.view.contracts.IAlbumListView;

public class AlbumListPresentation {

	private IAlbumListView albumListView;

	public AlbumListPresentation(IAlbumListView albumListView, List<Album> listAlbums) {
		this.albumListView = albumListView;
		fillDataView(listAlbums);
	}
	
	private void fillDataView(List<Album> albums) {
		albumListView.fillDataAlbumList(albums);
	}
	
}
