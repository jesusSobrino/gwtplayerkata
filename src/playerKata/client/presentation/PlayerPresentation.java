package playerKata.client.presentation;

import playerKata.client.view.contracts.IPlayerView;

public class PlayerPresentation {

	private IPlayerView iPlayer;

	public PlayerPresentation(IPlayerView playerView) {
		this.iPlayer = playerView;
		fillDataView();
	}

	private void fillDataView() {
		iPlayer.initialize();
	}
}
