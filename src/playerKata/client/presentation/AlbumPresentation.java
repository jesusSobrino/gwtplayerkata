package playerKata.client.presentation;

import playerKata.client.view.contracts.IAlbumView;

public class AlbumPresentation {

	private IAlbumView albumView;

	public AlbumPresentation(IAlbumView albumView) {
		this.albumView = albumView;
		fillDataView();
	}

	private void fillDataView() {
		albumView.initialize();
	}

}
