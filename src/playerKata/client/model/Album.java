package playerKata.client.model;

import java.util.List;

public class Album {

	private String id;
	private String tittle;
	private String artist;
	private String coverUrl;
	private List<Song> songs;

	public Album(String id, String tittle, String artist, String coverUrl, List<Song> songs) {
		this.id = id;
		this.tittle = tittle;
		this.artist = artist;
		this.coverUrl = coverUrl;
		this.songs = songs;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getCoverUrl() {
		return coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

}
