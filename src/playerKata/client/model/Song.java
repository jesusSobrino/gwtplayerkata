package playerKata.client.model;

public class Song {

	private String id;
	private String tittle;
	private String Url;

	public Song(String id, String tittle, String url) {
		this.id = id;
		this.tittle = tittle;
		Url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean isEqueals = true;

		Song other = (Song) obj;
		if (Url == null) {
			if (other.Url != null)
				isEqueals = false;
		} else if (!Url.equals(other.Url))
			isEqueals = false;
		if (id == null) {
			if (other.id != null)
				isEqueals = false;
		} else if (!id.equals(other.id))
			isEqueals = false;
		if (tittle == null) {
			if (other.tittle != null)
				isEqueals = false;
		} else if (!tittle.equals(other.tittle))
			isEqueals = false;

		return isEqueals;
	}

}
